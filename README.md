# Outliers

Package for easy to use outlier detection and removal using robust random cuts forest. RRCF implementation is based on https://github.com/kLabUM/rrcf.